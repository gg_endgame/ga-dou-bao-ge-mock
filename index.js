const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');
const shops = require('./shops.json');
const port = 8000;

const app = express();
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(
  cors({
    origin: '*',
  })
);

app.get('/init', (req, res) => {
  console.log('/init triggered');
  // res.status(500).send({});
  res.send({ version: 5 });
});
app.get('/shops', (req, res) => {
  console.log('/shops triggered');
  res.send(shops);
});

app.listen(port, () => console.log(`express hello world runing on ${port}`));
